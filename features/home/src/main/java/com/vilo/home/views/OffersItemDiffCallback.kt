package com.vilo.home.views

import androidx.recyclerview.widget.DiffUtil
import com.vilo.model.OffersModel

class OffersItemDiffCallback(private val oldList: List<OffersModel>,
                             private val newList: List<OffersModel>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
            = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
                && oldList[oldItemPosition].desc == newList[newItemPosition].desc
                && oldList[oldItemPosition].image == newList[newItemPosition].image
                && oldList[oldItemPosition].type == newList[newItemPosition].type
                && oldList[oldItemPosition].groupName == newList[newItemPosition].groupName
                && oldList[oldItemPosition].price == newList[newItemPosition].price
    }
}
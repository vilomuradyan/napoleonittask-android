package com.vilo.home.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import com.vilo.common.base.BaseViewModel
import com.vilo.common.utils.Event
import com.vilo.home.R
import com.vilo.home.domain.GetBannersUseCase
import com.vilo.home.domain.GetOffersUseCase
import com.vilo.model.BannersModel
import com.vilo.model.Offers
import com.vilo.model.OffersModel
import com.vilo.repository.AppDispatchers
import com.vilo.repository.utils.Resource
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(
    private val getBannersUseCase: GetBannersUseCase,
    private val getOffersUseCase: GetOffersUseCase,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    private val _offers = MediatorLiveData<Resource<List<OffersModel>>>()
    val offers: LiveData<Resource<List<OffersModel>>> get() = _offers
    private var offersSource: LiveData<Resource<List<OffersModel>>> = MutableLiveData()

    private val _banners = MediatorLiveData<Resource<List<BannersModel>>>()
    val banners: LiveData<Resource<List<BannersModel>>> get() = _banners
    private var bannersSource: LiveData<Resource<List<BannersModel>>> = MutableLiveData()

    private val _isTopTen = MutableLiveData<Boolean>()
    val isTopTen: LiveData<Boolean> get() = _isTopTen
    private val _isShop = MutableLiveData<Boolean>()
    val isShop: LiveData<Boolean> get() = _isShop
    private val _isProducts = MutableLiveData<Boolean>()
    val isProducts: LiveData<Boolean> get() = _isProducts

    init {
        _isTopTen.value = true
        getOffers(true)
        getBanners(true)
    }

    fun selectedTopTen() {
        _isTopTen.value = true
        _isProducts.value = false
        _isShop.value = false
    }

    fun selectedShop() {
        _isTopTen.value = false
        _isProducts.value = false
        _isShop.value = true
    }

    fun selectedProducts() {
        _isTopTen.value = false
        _isProducts.value = true
        _isShop.value = false
    }

    fun getOffers(offersModel: List<OffersModel>): List<Offers> {
        if (offersModel.isNotEmpty()) {
            val offers: MutableList<Offers> = ArrayList()
            offers.add(
                Offers(
                    id = offersModel[0].id, name = offersModel[0].name,
                    desc = offersModel[0].desc, groupName = offersModel[0].groupName,
                    type = offersModel[0].type, image = offersModel[0].image,
                    price = offersModel[0].price, discount = offersModel[0].discount,
                    viewType = 0, size = 1
                )
            )
            offers.add(
                Offers(
                    id = offersModel[0].id, name = offersModel[0].name,
                    desc = offersModel[0].desc, groupName = offersModel[0].groupName,
                    type = offersModel[0].type, image = offersModel[0].image,
                    price = offersModel[0].price, discount = offersModel[0].discount,
                    viewType = 1, size = 2
                )
            )
            var groupName = offers[0].groupName
            for (i in 1 until offersModel.size step 1) {
                if (offersModel[i].groupName == groupName) {
                    offers.add(
                        Offers(
                            id = offersModel[i].id, name = offersModel[i].name,
                            desc = offersModel[i].desc, groupName = offersModel[i].groupName,
                            type = offersModel[i].type, image = offersModel[i].image,
                            price = offersModel[i].price, discount = offersModel[i].discount,
                            viewType = 1, size = offers[offers.lastIndex].size + 1
                        )
                    )
                } else {
                    offers.add(
                        Offers(
                            id = offersModel[i].id, name = offersModel[i].name,
                            desc = offersModel[i].desc, groupName = offersModel[i].groupName,
                            type = offersModel[i].type, image = offersModel[i].image,
                            price = offersModel[i].price, discount = offersModel[i].discount,
                            viewType = 0, size = offers[offers.lastIndex].size + 1
                        )
                    )
                    offers.add(
                        Offers(
                            id = offersModel[i].id, name = offersModel[i].name,
                            desc = offersModel[i].desc, groupName = offersModel[i].groupName,
                            type = offersModel[i].type, image = offersModel[i].image,
                            price = offersModel[i].price, discount = offersModel[i].discount,
                            viewType = 1, size = offers[offers.lastIndex].size + 1
                        )
                    )
                    groupName = offersModel[i].groupName
                }
            }
            return offers
        } else {
            return ArrayList()
        }

    }

    private fun getOffers(forceRefresh: Boolean) = viewModelScope.launch(dispatchers.main) {
        _offers.removeSource(offersSource) // We make sure there is only one source of livedata (allowing us properly refresh)
        withContext(dispatchers.io) { offersSource = getOffersUseCase(forceRefresh = forceRefresh) }
        _offers.addSource(offersSource) {
            _offers.value = it
            if (it.status == Resource.Status.ERROR) _snackbarError.value =
                Event(R.string.an_error_happened)
        }
    }

    fun redirectInfoPage() = navigate(
        HomeFragmentDirections.actionHomeFragmentToDetailFragment("info")
    )

    private fun getBanners(forceRefresh: Boolean) = viewModelScope.launch(dispatchers.main) {
        _banners.removeSource(bannersSource) // We make sure there is only one source of livedata (allowing us properly refresh)
        withContext(dispatchers.io) {
            bannersSource = getBannersUseCase(forceRefresh = forceRefresh)
        }
        _banners.addSource(bannersSource) {
            _banners.value = it
            if (it.status == Resource.Status.ERROR) _snackbarError.value =
                Event(R.string.an_error_happened)
        }
    }

    fun configViewPager(): CompositePageTransformer {
        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(40))
        return compositePageTransformer
    }
}
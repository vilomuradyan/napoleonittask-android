package com.vilo.home.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vilo.home.databinding.ItemSlideContainerBinding
import com.vilo.model.BannersModel

class SliderViewHolder(itemView : View)
    : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemSlideContainerBinding.bind(itemView)

        fun bind(banners : BannersModel){
            binding.banners = banners
        }
}
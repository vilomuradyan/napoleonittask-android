package com.vilo.home.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vilo.home.databinding.ItemOffersBinding
import com.vilo.model.Offers

class OffersViewHolder(itemView : View)
    : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemOffersBinding.bind(itemView)
        fun bind(offers: Offers){
            binding.offers = offers
        }
}
package com.vilo.home.binding

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.vilo.home.R
import com.vilo.home.adapter.OffersAdapter
import com.vilo.home.adapter.SliderAdapter
import com.vilo.home.views.HomeViewModel
import com.vilo.model.BannersModel
import com.vilo.model.OffersModel
import com.vilo.repository.utils.Resource
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator


object HomeBinding {

    @BindingAdapter(":imageUrl")
    @JvmStatic
    fun loadImage(image: RoundedImageView, url: String) {
        Glide.with(image.context).load(url).into(image)
    }

    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImageUrl(image: AppCompatImageView, url: String) {
        Glide.with(image.context).load(url).into(image)
    }

    @BindingAdapter(value = ["app:items", "app:config"])
    @JvmStatic
    fun setItems(
        viewPager2: ViewPager2,
        resource: Resource<List<BannersModel>>?,
        compositePageTransformer: CompositePageTransformer
    ) {
        with(viewPager2.adapter as SliderAdapter) {
            resource?.data?.let { updateData(it) }
            viewPager2.offscreenPageLimit = 3
            viewPager2.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            viewPager2.setPageTransformer(compositePageTransformer)
        }
    }

    @BindingAdapter(value = ["app:items", "app:viewModel"])
    @JvmStatic
    fun setItem(
        recyclerView: RecyclerView, resource: Resource<List<OffersModel>>?,
        viewModel: HomeViewModel
    ) {
        with(recyclerView.adapter as OffersAdapter) {
            resource?.data?.let {
                updateData(viewModel.getOffers(it))
            }
        }

        val itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            0, ItemTouchHelper.LEFT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                with(recyclerView.adapter as OffersAdapter){
                    removeItem(viewHolder)
                }
            }
            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val backgroundColor = ColorDrawable(Color.parseColor("#FF0000"))
                val deleteIcon : Drawable = ContextCompat.getDrawable(recyclerView.context, R.drawable.ic_delete_new)!!
                val itemView = viewHolder.itemView
                val iconMargin = (itemView.height - deleteIcon.intrinsicWidth) / 2
                if (dX > 0){
                    backgroundColor.setBounds(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                    deleteIcon.setBounds(itemView.left + iconMargin,itemView.top + iconMargin, itemView.left + iconMargin + deleteIcon.intrinsicWidth,
                    itemView.bottom - iconMargin)
                }else{
                    backgroundColor.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                    deleteIcon.setBounds(itemView.right - iconMargin - deleteIcon.intrinsicWidth,itemView.top + iconMargin, itemView.right - iconMargin,
                        itemView.bottom - iconMargin)
                }
                backgroundColor.draw(c)
                c.save()

                if (dX > 0){
                    c.clipRect(itemView.left, itemView.top, dX.toInt(), itemView.bottom)

                }else{
                    c.clipRect(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                }
                deleteIcon.draw(c)
                c.restore()
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }

        })
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    @BindingAdapter("app:strike")
    @JvmStatic
    fun setFlags(textView: AppCompatTextView, state: Boolean) {
        if (state) {
            textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
    }
}
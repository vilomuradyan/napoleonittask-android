package com.vilo.home.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.vilo.model.OffersModel
import com.vilo.repository.Repository
import com.vilo.repository.utils.Resource

class GetOffersUseCase(private val repository: Repository) {

    suspend operator fun invoke(forceRefresh: Boolean = false): LiveData<Resource<List<OffersModel>>> {
        return Transformations.map(repository.getOffersWithCache(forceRefresh)) { it ->
            it // Place here your specific logic actions
        }
    }
}
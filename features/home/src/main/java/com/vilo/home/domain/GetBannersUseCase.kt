package com.vilo.home.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.vilo.model.BannersModel
import com.vilo.repository.Repository
import com.vilo.repository.utils.Resource

/**
 * Use case that gets a [Resource][List][Search] from [BatmanRepository]
 * and makes some specific logic actions on it.
 *
 * In this Use Case, I'm just doing nothing... ¯\_(ツ)_/¯
 */
class GetBannersUseCase(private val repository: Repository) {
    suspend operator fun invoke(forceRefresh: Boolean = false): LiveData<Resource<List<BannersModel>>> {
        return Transformations.map(repository.getBannersWithCache(forceRefresh)) {
            it // Place here your specific logic actions
        }
    }
}
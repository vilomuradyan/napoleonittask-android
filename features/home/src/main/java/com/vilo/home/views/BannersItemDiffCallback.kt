package com.vilo.home.views

import androidx.recyclerview.widget.DiffUtil
import com.vilo.model.BannersModel

class BannersItemDiffCallback(private val oldList: List<BannersModel>,
                              private val newList: List<BannersModel>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
            = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
                && oldList[oldItemPosition].desc == newList[newItemPosition].desc
                && oldList[oldItemPosition].image == newList[newItemPosition].image
                && oldList[oldItemPosition].title == newList[newItemPosition].title
    }
}
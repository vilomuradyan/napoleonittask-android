package com.vilo.home.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.vilo.common.base.BaseFragment
import com.vilo.common.base.BaseViewModel
import com.vilo.home.adapter.OffersAdapter
import com.vilo.home.adapter.SliderAdapter
import com.vilo.home.databinding.FragmentHomeBinding
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment(){

    private val homeViewModel : HomeViewModel by viewModel()
    private lateinit var binding: FragmentHomeBinding
    override fun getViewModel(): BaseViewModel = homeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewModel = homeViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureViewPager()
        configRecycler()
    }

    private fun configureViewPager(){
        binding.viewpager2.adapter = SliderAdapter()
    }

    private fun configRecycler(){
        binding.recyclerView.adapter = OffersAdapter()
    }
}
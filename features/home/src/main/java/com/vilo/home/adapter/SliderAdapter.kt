package com.vilo.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.vilo.home.R
import com.vilo.home.viewHolder.SliderViewHolder
import com.vilo.home.views.BannersItemDiffCallback
import com.vilo.model.BannersModel

class SliderAdapter : RecyclerView.Adapter<SliderViewHolder>() {

    private val banners: MutableList<BannersModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder =
        SliderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_slide_container, parent, false
            )
        )

    override fun onBindViewHolder(holder: SliderViewHolder, position: Int) =
        holder.bind(banners = banners[position])

    override fun getItemCount(): Int = banners.size

    fun updateData(items: List<BannersModel>) {
        val diffCallback = BannersItemDiffCallback(banners, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        banners.clear()
        banners.addAll(items)

        diffResult.dispatchUpdatesTo(this)
    }
}
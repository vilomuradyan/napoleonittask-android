package com.vilo.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vilo.home.R
import com.vilo.home.viewHolder.GroupViewHolder
import com.vilo.home.viewHolder.OffersViewHolder
import com.vilo.model.Offers

class OffersAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val offers: MutableList<Offers> = mutableListOf()

    override fun getItemViewType(position: Int): Int{
        return offers[position].viewType
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            0 -> {
                GroupViewHolder(layoutInflater.inflate(R.layout.item_groups,
                    parent, false))
            }
            1 -> {
                OffersViewHolder(layoutInflater.inflate(R.layout.item_offers,
                    parent, false))
            }
            else -> {
                val viewGroup : ViewGroup = layoutInflater.inflate(R.layout.item_groups,
                    parent, false) as ViewGroup
                GroupViewHolder(viewGroup)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is GroupViewHolder) {
            holder.bind(offers[position])
        } else if (holder is OffersViewHolder) {
            holder.bind(offers[position])
        }
    }

    fun removeItem(viewHolder : RecyclerView.ViewHolder){
        offers.removeAt(viewHolder.adapterPosition)
        notifyItemRemoved(viewHolder.adapterPosition)
    }

    fun updateData(items: List<Offers>) {
        offers.clear()
        offers.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = offers.size
}
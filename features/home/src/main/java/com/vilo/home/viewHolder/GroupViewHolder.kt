package com.vilo.home.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vilo.home.databinding.ItemGroupsBinding
import com.vilo.model.Offers
import com.vilo.model.OffersModel

class GroupViewHolder(itemView : View)
    : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemGroupsBinding.bind(itemView)

        fun bind(offers: Offers){
            binding.offers = offers
        }
}
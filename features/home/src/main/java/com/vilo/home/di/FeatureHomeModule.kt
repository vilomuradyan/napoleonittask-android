package com.vilo.home.di

import com.vilo.home.domain.GetBannersUseCase
import com.vilo.home.domain.GetOffersUseCase
import com.vilo.home.views.HomeViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val featureHomeModule = module {
    factory { GetOffersUseCase(get()) }
    factory { GetBannersUseCase(get()) }
    viewModel { HomeViewModel(get(), get(), get()) }
}
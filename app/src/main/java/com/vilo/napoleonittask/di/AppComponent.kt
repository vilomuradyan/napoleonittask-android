package com.vilo.napoleonittask.di

import com.vilo.home.di.featureHomeModule
import com.vilo.local.di.localModule
import com.vilo.remote.di.createRemoteModule
import com.vilo.repository.di.repositoryModule

val appComponent = listOf(
    createRemoteModule("https://s3.eu-central-1.amazonaws.com/sl.files/"),
    repositoryModule,
    featureHomeModule,
    localModule
)
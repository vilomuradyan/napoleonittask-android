package com.vilo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "banners")
data class BannersModel (

	@PrimaryKey
	@ColumnInfo(name = "id")
	@SerializedName("id") val id : String,

	@ColumnInfo(name = "image")
	@SerializedName("image") val image : String?,

	@ColumnInfo(name = "title")
	@SerializedName("title") val title : String?,

	@ColumnInfo(name = "desc")
	@SerializedName("desc") val desc : String?
)
package com.vilo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "offers")
data class OffersModel(
    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id") val id : String,

    @ColumnInfo(name = "name")
    @SerializedName("name") val name : String?,

    @ColumnInfo(name = "desc")
    @SerializedName("desc") val desc : String?,

    @ColumnInfo(name = "group_name")
    @SerializedName("groupName") val groupName : String?,

    @ColumnInfo(name = "type")
    @SerializedName("type") val type : String?,

    @ColumnInfo(name = "image")
    @SerializedName("image") val image : String?,

    @ColumnInfo(name = "price")
    @SerializedName("price") val price : Float?,

    @ColumnInfo(name = "discount")
    @SerializedName("discount") val discount : Float?
)

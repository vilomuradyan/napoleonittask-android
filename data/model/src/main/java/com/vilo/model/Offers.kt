package com.vilo.model

data class Offers(
    val id: String,
    val name: String?,
    val desc: String?,
    val groupName: String?,
    val type: String?,
    val image: String?,
    val price: Float?,
    val discount: Float?,
    var viewType : Int,
    var size : Int = 0
)
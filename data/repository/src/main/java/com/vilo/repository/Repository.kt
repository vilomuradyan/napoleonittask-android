package com.vilo.repository

import androidx.lifecycle.LiveData
import com.vilo.local.dao.BannersDao
import com.vilo.local.dao.OffersDao
import com.vilo.model.BannersModel
import com.vilo.model.OffersModel
import com.vilo.remote.DataSours
import com.vilo.repository.utils.NetworkBoundResource
import com.vilo.repository.utils.Resource
import kotlinx.coroutines.Deferred

interface Repository {

    suspend fun getOffersWithCache(forceRefresh: Boolean)
            : LiveData<Resource<List<OffersModel>>>

    suspend fun getBannersWithCache(forceRefresh: Boolean)
            : LiveData<Resource<List<BannersModel>>>

}
    class RepositoryImpl(
        private val dataSours: DataSours,
        private val offersDao: OffersDao,
        private val bannersDao: BannersDao
    ) : Repository {
        override suspend fun getOffersWithCache(forceRefresh: Boolean): LiveData<Resource<List<OffersModel>>> {
            return object : NetworkBoundResource<List<OffersModel>, List<OffersModel>>() {
                override fun processResponse(response: List<OffersModel>): List<OffersModel> =
                    response.sortedBy { it.groupName }

                override suspend fun saveCallResults(items: List<OffersModel>) {
                    items.forEach { offersDao.saveOffers(it) }
                }

                override fun shouldFetch(data: List<OffersModel>?): Boolean =
                    data == null || data.isEmpty() || forceRefresh

                override suspend fun loadFromDb(): List<OffersModel> = offersDao.getOffers().sortedBy { it.groupName }

                override fun createCallAsync(): Deferred<List<OffersModel>> =
                    dataSours.getOffersAsync()

            }.build().asLiveData()
        }

        override suspend fun getBannersWithCache(forceRefresh: Boolean): LiveData<Resource<List<BannersModel>>> {
            return object : NetworkBoundResource<List<BannersModel>, List<BannersModel>>() {
                override fun processResponse(response: List<BannersModel>): List<BannersModel> =
                    response

                override suspend fun saveCallResults(items: List<BannersModel>) {
                    items.forEach { bannersDao.saveBanners(it) }
                }

                override fun shouldFetch(data: List<BannersModel>?): Boolean =
                    data == null || data.isEmpty() || forceRefresh

                override suspend fun loadFromDb(): List<BannersModel>
                = bannersDao.getBanners()


                override fun createCallAsync(): Deferred<List<BannersModel>>
                = dataSours.getBannersAsync()

            }.build().asLiveData()
        }
    }
package com.vilo.repository.di

import com.vilo.repository.AppDispatchers
import com.vilo.repository.Repository
import com.vilo.repository.RepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module.module

val repositoryModule = module {
    factory { AppDispatchers(Dispatchers.Main, Dispatchers.IO) }
    factory { RepositoryImpl(get(), get(), get()
    ) as Repository }
}
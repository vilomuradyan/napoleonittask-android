package com.vilo.local.di

import com.vilo.local.AppDataBase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

private const val DATABASE = "DATABASE"

val localModule = module {
    single(DATABASE) { AppDataBase.buildDatabase(androidContext()) }
    factory { (get(DATABASE) as AppDataBase).bannersDao()}
    factory { (get (DATABASE) as AppDataBase).offersDao()}
}
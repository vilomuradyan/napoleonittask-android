package com.vilo.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.vilo.local.dao.BannersDao
import com.vilo.local.dao.OffersDao
import com.vilo.model.BannersModel
import com.vilo.model.OffersModel

@Database(entities = [OffersModel::class, BannersModel::class], version = 3, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {

    // DAO
    abstract fun bannersDao() : BannersDao
    abstract fun offersDao() : OffersDao

    companion object {

        fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, AppDataBase::class.java, "ArchApp.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}
package com.vilo.local.dao
import androidx.room.*
import com.vilo.model.OffersModel

@Dao
interface OffersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveOffers(offersModel: OffersModel)

    @Query("select * from offers")
    suspend fun getOffers() : List<OffersModel>

    @Update
    suspend fun updateOffers(offersModel: OffersModel)

    @Delete
    suspend fun deleteOffers(offersModel: OffersModel)
}
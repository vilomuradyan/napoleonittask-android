package com.vilo.local.dao

import androidx.room.*
import com.vilo.model.BannersModel

@Dao
interface BannersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveBanners(bannersModel: BannersModel)

    @Query("select * from banners")
    suspend fun getBanners() : List<BannersModel>

    @Update
    suspend fun updateBanners(bannersModel: BannersModel)

    @Delete
    suspend fun deleteBanners(bannersModel: BannersModel)
}
package com.vilo.remote
import com.vilo.model.BannersModel
import com.vilo.model.OffersModel
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface ApiService {

    @GET("offers.json")
    fun getOffersAsync() : Deferred<List<OffersModel>>

    @GET("banners.json")
    fun getBannersAsync() : Deferred<List<BannersModel>>
}
package com.vilo.remote

class DataSours(private val apiService: ApiService) {

    fun getOffersAsync() =
        apiService.getOffersAsync()

    fun getBannersAsync() =
        apiService.getBannersAsync()
}
package com.vilo.common.utils

import android.content.Context
import android.graphics.PointF
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

class LinearLayoutManagerWithSmoothScroll(context: Context) : LinearLayoutManager(context) {

    override fun smoothScrollToPosition(
        recyclerView: RecyclerView?,
        state: RecyclerView.State?,
        position: Int
    ) {
        val smoothScroller = recyclerView?.context?.let { TopSnappedSmoothScroll(it) }
        position.let { smoothScroller?.targetPosition = it }
        startSmoothScroll(smoothScroller)
    }

    inner class TopSnappedSmoothScroll(context: Context) : LinearSmoothScroller(context) {

        override fun computeScrollVectorForPosition(targetPosition: Int): PointF?
        = this@LinearLayoutManagerWithSmoothScroll.computeScrollVectorForPosition(targetPosition)

        override fun getVerticalSnapPreference(): Int = SNAP_TO_START
    }
}